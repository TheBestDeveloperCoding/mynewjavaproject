package non.recursion.method;

public class NonRecursion {

        //nonrecursive(iterative) version
        public int iterativelyCalculateFactorial(int i){
            if (i==1 || i==0){
                return 1;
            }
            else{
                 int fact=1;
                for(int j=1;j<=i; j++){
                    fact=fact*j;
                }
                return fact;
            }
        }
}
