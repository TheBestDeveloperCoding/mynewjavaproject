package main.method;
import non.recursion.method.NonRecursion;
import recursion.method.Recursion;
import java.util.Scanner;

public class MainClass {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String args[]){
        Recursion recursion = new Recursion();
        System.out.print("Enter a value for recursion method: ");
        int recursionNum = scanner.nextInt();
        int result1 = recursion.recursivelyCalculateFactorial(recursionNum);
        System.out.println("Factorial of " + recursionNum + " is equal to " + result1 + " for recursion method");
        NonRecursion nonRecursion = new NonRecursion();
        System.out.print("Enter a value for nonrecursion method: ");
        int nonrecursionNum = scanner.nextInt();
        int result2 = nonRecursion.iterativelyCalculateFactorial(nonrecursionNum);
        System.out.println("Factorial of " + nonrecursionNum + " is equal to " + result2 + " for nonrecursion method");
    }
}
