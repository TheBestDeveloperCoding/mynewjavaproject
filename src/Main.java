import java.io.*;

public class Main {
    public static void main(String[] args) {
        CreatedFile file = new CreatedFile();
        file.callingFile();
        WriteFile writeFile = new WriteFile();
        writeFile.callingWriteFile();
        ReadFile readFile = new ReadFile();
        readFile.callingReadFile();
        TakeAllInformations take = new TakeAllInformations();
        take.takeInformation();
        // DeleteFile deleteFile = new DeleteFile();
        // deleteFile.callingDeleteFile();
    }
}

class CreatedFile {
    public void callingFile() {
        try {
            File smth = new File("text.txt");
            if (smth.createNewFile()) {
                System.out.println("File is created = " + smth.getName());
            } else {
                System.out.println("File is already existed....");
            }
        } catch (IOException exception) {
            System.out.println("Something is wrong");
            exception.printStackTrace();
        }
    }
}

class WriteFile {
    public void callingWriteFile() {
        try {
            Writer writer = new FileWriter("text.txt");
            writer.write("Valekum assalomu va rohmatullohi va barokatuh....");
            writer.close();
            System.out.println("Succesfully is written to file....");
        } catch (IOException exception) {
            System.out.println("Something is wrong");
            exception.printStackTrace();
        }
    }
}

class ReadFile {
    public void callingReadFile() {
        char[] input = new char[100];
        try {
            Reader readFile = new FileReader("text.txt");
           /* Scanner read = new Scanner(readFile);
            while (read.hasNextLine()) {
                String inform = read.nextLine();
                System.out.println(inform);
            }*/
            readFile.read(input,5,12);
            System.out.println(input);
            System.out.println("Succesfully is read file....");
            readFile.close();
        } catch (IOException notFound) {
            System.out.println("Something is wrong....");
            notFound.printStackTrace();
        }
    }
}

class TakeAllInformations {
    public void takeInformation() {
        File inform = new File("text.txt");
        if (inform.exists()) {
            System.out.println("File name: " + inform.getName());
            System.out.println("Absolute path: " + inform.getAbsolutePath());
            System.out.println("Writeable: " + inform.canWrite());
            System.out.println("Readable " + inform.canRead());
            System.out.println("File size in bytes " + inform.length());
        } else {
            System.out.println("File is not existing....");
        }
    }
}
/*
class DeleteFile {
    public void callingDeleteFile() {
        File delete = new File("text.txt");
        if (delete.delete()) {
            System.out.println(delete.getName() + " is deleted....");
        } else {
            System.out.println("Failed for deleting....");
        }
    }
}*/