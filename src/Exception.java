/*import java.io.*;
class Exception {
    public void findFile() throws IOException {
        throw new IOException("File not found");
    }

    public static void main(String[] args) {
        Exception exception = new Exception();
        try {
            exception.findFile();
            System.out.println("Rest of code in try block");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
*/
class Exception {
    public static void main(String[] args) {
        try {
            int array[] = new int[10];
            array[12] = 7;
            array[9] = 30 / 0;
        } catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }
    }
}