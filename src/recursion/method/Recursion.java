package recursion.method;

public class Recursion {

    int fact;
    //recursive version
    public int recursivelyCalculateFactorial(int i){
        fact=1;
        if (i==1 || i==0)
            return 1;
        else{
            fact=i*recursivelyCalculateFactorial(i-1);
            return fact;
        }
    }

}
